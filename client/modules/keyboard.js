import { Mesh, BoxGeometry, MeshStandardMaterial, TextureLoader } from 'three';

export default class Keyboard {
  constructor() {
    const geometry = new BoxGeometry(0.45, 0.01, 0.135, 4, 2, 1);
    const material = new MeshStandardMaterial({ wireframe: false });
    const map = new TextureLoader().load('./assets/images/keyboard.png');
    map.repeat.x = 1;
    map.repeat.y = 0.28;
    map.offset.y = 0.36;
    material.map = map;
    this.object = new Mesh(geometry, material);
    this.object.name = 'keyboard';
    this.object.castShadow = true;
    this.object.receiveShadow = true;
    this.object.position.set(-0.26, 1.115, -0.05);

    setTimeout(() => {
      if (window.location.origin.indexOf('localhost') === -1) {
        window.desktop.connection.socket.on('keyup', (e) => { console.log(`keyup: ${e.data}`); });
        window.desktop.connection.socket.on('keydown', (e) => { console.log(`keydown: ${e.data}`); });
      }
    }, 1000);
  }
}
